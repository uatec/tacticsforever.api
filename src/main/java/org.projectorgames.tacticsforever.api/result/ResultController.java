package org.projectorgames.tacticsforever.api.result;

import org.joda.time.DateTime;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Api(description = "Store the results of battles and retrieve other ships results and aggregate data of these battle results. ", name = "Result Service")
@RestController
public class ResultController {

    final static Logger logger = LoggerFactory.getLogger(ResultController.class);
    @Autowired
    ResultRepository resultRepository;
    @Autowired
    MongoTemplate mongoTemplate;

    private static <T, K, R> R getMostCommon(List<T> source, Function<T, K> classifier) {
        Object[] results = source.stream()
                .collect(Collectors.groupingBy(classifier))
                .values()
                .stream()
                .sorted((a, b) -> Integer.compare(a.size(), b.size()))
                .limit(1)
                .map(l -> classifier.apply(l.stream().findFirst().get()))
                .toArray();

        if (results.length == 1) {
            return (R) results[0];
        } else {
            return null;
        }
    }

    @ApiMethod(description = "Displays all results ever posted.", consumes = {"application/json"})
    @RequestMapping(value = "/results", method = {RequestMethod.GET})
    public
    @ApiResponseObject
    List<Result> get() {
        return resultRepository.findAll();
    }

    @RequestMapping(value = "/results/summaries", method = {RequestMethod.GET})
    public List<ResultSummary> getSummaries(@RequestParam(value = "ships", defaultValue = "") String ships) {
        String[] shipIds = ships.split("\\|");
        List<ResultSummary> summaries = new ArrayList<>();

        for (String shipId : shipIds) {
            summaries.add(getShipResultsSummary(shipId));
        }

        return summaries;
    }

    @RequestMapping(value = "/results/{shipId}", method = {RequestMethod.GET})
    public List<Result> getShipResults(@PathVariable String shipId) {

        List<WinLoseResult> winResults = mongoTemplate.find(new Query(Criteria.where("winnerId").is(shipId).and("_class").is(WinLoseResult.class.getCanonicalName())), WinLoseResult.class, "result");
        List<WinLoseResult> loseResults = mongoTemplate.find(new Query(Criteria.where("loserId").is(shipId).and("_class").is(WinLoseResult.class.getCanonicalName())), WinLoseResult.class, "result");
        List<DrawResult> drawnResultsA = mongoTemplate.find(new Query(Criteria.where("drawerId1").is(shipId).and("_class").is(DrawResult.class.getCanonicalName())), DrawResult.class, "result");
        List<DrawResult> drawnResultsB = mongoTemplate.find(new Query(Criteria.where("drawerId2").is(shipId).and("_class").is(DrawResult.class.getCanonicalName())), DrawResult.class, "result");

        List<Result> allResults = new ArrayList<>();
        allResults.addAll(winResults);
        allResults.addAll(loseResults);
        allResults.addAll(drawnResultsA);
        allResults.addAll(drawnResultsB);

        return allResults;
    }

    @RequestMapping(value = "/results", method = {RequestMethod.POST})
    public void post(@RequestBody ResultDTO resultDto) throws Exception {
        Result result = null;
        switch (resultDto.getResult()) {
            case "winlose":
                result = new WinLoseResult(resultDto.getWinnerId(),
                        resultDto.getLoserId());
                break;
            case "draw":
                result = new DrawResult(resultDto.getWinnerId(),
                        resultDto.getLoserId());
                break;
        }

        if (result == null) {
            throw new Exception(String.format("Unknown result type '%s'", resultDto.getResult()));
        }

        result.createdDateTime = DateTime.now();
        resultRepository.save(result);
    }

    @RequestMapping(value = "/results/{shipId}/summary", method = {RequestMethod.GET})
    public ResultSummary getShipResultsSummary(@PathVariable String shipId) {

        List<WinLoseResult> winResults = mongoTemplate.find(new Query(Criteria.where("winnerId").is(shipId).and("_class").is(WinLoseResult.class.getCanonicalName())), WinLoseResult.class, "result");
        List<WinLoseResult> loseResults = mongoTemplate.find(new Query(Criteria.where("loserId").is(shipId).and("_class").is(WinLoseResult.class.getCanonicalName())), WinLoseResult.class, "result");
        List<DrawResult> drawnResultsA = mongoTemplate.find(new Query(Criteria.where("drawerId1").is(shipId).and("_class").is(DrawResult.class.getCanonicalName())), DrawResult.class, "result");
        List<DrawResult> drawnResultsB = mongoTemplate.find(new Query(Criteria.where("drawerId2").is(shipId).and("_class").is(DrawResult.class.getCanonicalName())), DrawResult.class, "result");

        return new ResultSummary(shipId, winResults.size(), loseResults.size(), drawnResultsA.size() + drawnResultsB.size(),
                getMostCommon(winResults, WinLoseResult::getLoserId),
                getMostCommon(loseResults, WinLoseResult::getWinnerId));
    }
}

