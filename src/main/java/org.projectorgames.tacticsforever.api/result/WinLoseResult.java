package org.projectorgames.tacticsforever.api.result;

public class WinLoseResult extends Result {
    private final String winnerId;
    private final String loserId;

    public WinLoseResult(String winnerId, String loserId) {

        this.winnerId = winnerId;
        this.loserId = loserId;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public String getLoserId() {
        return loserId;
    }
}
