package org.projectorgames.tacticsforever.api.result;

public class ResultSummary {

    private final String playerId;
    private final int wins;
    private final int losses;
    private final int draws;
    private final String nemesisId;
    private final String victimId;

    public ResultSummary(String playerId, int wins, int losses, int draws, String nemesisId, String victimId) {
        this.playerId = playerId;

        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
        this.nemesisId = nemesisId;
        this.victimId = victimId;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public int getDraws() {
        return draws;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getNemesisId() {
        return nemesisId;
    }

    public String getVictimId() {
        return victimId;
    }
}
