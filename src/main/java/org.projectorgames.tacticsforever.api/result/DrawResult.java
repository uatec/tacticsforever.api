package org.projectorgames.tacticsforever.api.result;

public class DrawResult extends Result {
    private final String drawerId1;
    private final String drawerId2;

    public DrawResult(String drawerId1, String drawerId2) {

        this.drawerId1 = drawerId1;
        this.drawerId2 = drawerId2;
    }

    public String getDrawerId1() {
        return drawerId1;
    }

    public String getDrawerId2() {
        return drawerId2;
    }
}
