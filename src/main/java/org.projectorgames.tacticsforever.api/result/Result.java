package org.projectorgames.tacticsforever.api.result;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;

public class Result {
    public DateTime createdDateTime;
    @Id
    public String id;

}
