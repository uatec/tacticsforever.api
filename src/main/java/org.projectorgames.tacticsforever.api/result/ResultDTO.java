package org.projectorgames.tacticsforever.api.result;

public class ResultDTO {
    private String result;
    private String winnerId;
    private String loserId;

    public ResultDTO() {
    }

    public ResultDTO(String result, String winnerId, String loserId) {
        this.result = result;
        this.winnerId = winnerId;
        this.loserId = loserId;
    }

    public String getLoserId() {
        return loserId;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public String getResult() {
        return result;
    }
}
