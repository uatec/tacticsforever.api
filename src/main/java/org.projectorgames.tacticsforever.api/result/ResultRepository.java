package org.projectorgames.tacticsforever.api.result;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ResultRepository extends MongoRepository<Result, String> {

}
