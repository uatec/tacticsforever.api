package org.projectorgames.tacticsforever.api.ship;

import org.joda.time.DateTime;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;
import org.springframework.data.annotation.Id;

@ApiObject
public class Ship {

    @ApiObjectField(description = "When the ship was created or updated.")
    private DateTime createdDatetime;
    @Id
    @ApiObjectField(description = "The globally unique identifier of this ship. If not provided, this will be assigned automatically.")
    private String id;
    @ApiObjectField(description = "The human friendly name of this ship.")
    private String name;
    @ApiObjectField(description = "The binary data which represents the actual ship components and configuration.", required = true)
    private byte[] data;
    @ApiObjectField(description = "A list of tags by which this ship can be found or grouped.")
    private String[] tags;

    public Ship() {

    }

    public Ship(DateTime createdDatetime, String id, String name, byte[] data, String[] tags) {
        this.createdDatetime = createdDatetime;
        this.id = id;
        this.name = name;
        this.data = data;
        this.tags = tags;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] value) {
        this.data = value;
    }

    public String[] getTags() {
        return this.tags;
    }

    public void setTags(String[] value) {
        this.tags = value;
    }

    @Override
    public String toString() {
        return String.format("Ship[id=%s, name=%s, tags=%s]",
                this.id,
                this.name,
                this.tags != null ? toString(this.tags) : "null");
    }

    private String toString(String[] array) {
        StringBuilder builder = new StringBuilder();
        builder.append('[');

        for (String var : array) {
            builder.append(var);
            builder.append(", ");
        }

        if (array.length > 0) // remove trailing space and comma, messy I know.
        {
            builder.deleteCharAt(builder.length() - 1);
            builder.deleteCharAt(builder.length() - 1);
        }
        builder.append(']');
        return builder.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DateTime getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(DateTime value) {
        this.createdDatetime = value;
    }
}
