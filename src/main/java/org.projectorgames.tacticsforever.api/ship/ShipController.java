package org.projectorgames.tacticsforever.api.ship;

import org.joda.time.DateTime;
import org.jsondoc.core.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@Api(description = "Save and retrieve ships", name = "Ship Service")
@RestController
public class ShipController {
    final static Logger logger = LoggerFactory.getLogger(ShipController.class);
    @Autowired
    ShipRepository shipRepository;
    @Autowired
    MongoTemplate mongoTemplate;

    @ApiMethod(description = "Stores the uploaded ship, overwriting any ship that already exists with the given Id.", consumes = {"application/json"})
    @RequestMapping(value = "/ships", method = {RequestMethod.POST})
    public
    @ApiResponseObject
    String post(@ApiBodyObject @RequestBody Ship ship) throws Exception {

        if (ship.getData() == null || ship.getData().length == 0) {
            throw new Exception("'data' is a mandatory field on an object of type 'Ship'.");
        }

        logger.info("{\"event\" : \"request\", \"path\": \"/ships\", \"method\": \"POST\"}");
        ship.setCreatedDatetime(DateTime.now());
        ship = shipRepository.save(ship);

        return "/ship/" + ship.getName();
    }

    @ApiMethod(description = "Get groups of ships.", produces = {"application/json"})
    @RequestMapping(value = "/ships", method = {RequestMethod.GET})
    public
    @ApiResponseObject
    List<Ship> index(@ApiQueryParam(name = "tags", description = "A pipe separated list of tags which must be present in all ships to be returned.")
                     @RequestParam(value = "tags", defaultValue = "")
                     String tags,
                     @ApiQueryParam(name = "delete", description = "True if the ships retrieved should be deleted from the datastore along with this request.")
                     @RequestParam(value = "delete", defaultValue = "false")
                     Boolean delete) {

        logger.info("{\"event\" : \"request\", \"path\": \"/ships?tags=" + tags + "&delete=" + delete + "\", \"method\": \"GET\"}");

        if ( tags.length() == 0  )
        {
            return shipRepository.findAll();
        }

        String[] splitTags = tags.split("\\|");
        if ( delete )
        {
            return mongoTemplate.findAllAndRemove(
                    new Query(buildTagCriteria(splitTags)),
                    Ship.class);
        }
        else
        {
            return mongoTemplate.find(
                    new Query(buildTagCriteria(splitTags)),
                    Ship.class);
        }
    }

    private Criteria buildTagCriteria(String[] tags)
    {
        List<Criteria> tagCriteria = new ArrayList<>();

        for ( String tag : tags)
        {
            tagCriteria.add(Criteria.where("tags").in(tag));
        }

        return new Criteria().andOperator(tagCriteria.toArray(new Criteria[1]));
    }

    @ApiMethod(description = "Get the data and tags for an individual ship, given it's globally unique id.", produces = {"application/json"})
    @RequestMapping(value = "/ship/{id}", method = {RequestMethod.GET})
    public
    @ApiResponseObject
    ResponseEntity<Ship> get(@ApiPathParam(name = "id", description = "The globally unique ID of the ships to be retrieved.")
                             @PathVariable
                             String id) {

        logger.info("{\"event\" : \"request\", \"path\": \"/ship/" + id + "\", \"method\": \"GET\"}");

        Ship ship = shipRepository.findOne(id);

        if (ship == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(ship, HttpStatus.OK);
    }
}
