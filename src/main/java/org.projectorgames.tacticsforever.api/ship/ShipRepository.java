package org.projectorgames.tacticsforever.api.ship;


import org.springframework.data.mongodb.repository.MongoRepository;

public interface ShipRepository extends MongoRepository<Ship, String> {

}

