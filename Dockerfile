FROM maven:3.2-jdk-8-onbuild
ENTRYPOINT ["java", "-jar", "target/tacticsforeverapi-0.1.0.war"]