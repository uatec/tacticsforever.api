# README #

## What is this repository for? ##

* Version 0.2 (Warning: Version numbers are merely documentation annotations, not verifiable programmatically anywhere)
* This API allows TacticsForever to store and retrieve ships in the cloud.
* Future features include ranking and analytics.

## How do I get set up? ##

### Prerequisites ###
 
Development:

* Java 1.8
* Maven

Build:

* Docker

Run:

* MongoDB

### Configuration ###

There are no custom configuration options. Platform configuration can be performed by modifying application.properties or setting environment variables according to the [Spring Boot documentation](http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html)

### Development Instructions ###

Build and run locally using: mvn spring-boot:run

### Deployment Instructions ###

This API is deployed as a docker container. This container image can be built using: 

> docker build -t uatec/tacticsforever.api .

> docker push uatec/tacticsforever.api

Then you must instruct your docker hosts to pull the updated image before restarting the container.

## Who do I talk to? ##

* Uatec [uatecuk@gmail.com](uatecuk@gmail.com)
* ProjectorGames [info@projectorgames.net](info@projectorgames.net)